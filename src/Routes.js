import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import useAmplifyAuth from "./customHooks/useAmplifyAuth";
import Bitacora from "./components/Bitacora/Bitacora";

const Routes = () => {
  const {
    state: { user, isLoading }
  } = useAmplifyAuth();

  if (isLoading) return <p>Loading</p>;
  return (
    <Router>
      <Switch>
        {user ? (
          <>
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/bitacora/:SK" component={Bitacora} />
          </>
        ) : (
          <>
            <Route path="/login" component={Login} />
            <Redirect to="/login" />
          </>
        )}
      </Switch>
    </Router>
  );
};

export default Routes;
