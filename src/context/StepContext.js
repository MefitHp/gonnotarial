import { useState, useEffect } from "react";

function HandleCaseFileProgress(caseFile = {}) {
  const { phase, status } = caseFile;
  const [activeStep, setActiveStep] = useState(0);

  useEffect(() => {
    handleProgress(caseFile);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [caseFile]);

  let handleProgress = () => {
    switch (phase) {
      case "BEGIN":
        switch (status) {
          case "CREATED":
            return setActiveStep(0);
          default:
            return "Error, status inexistente";
        }
      case "AUTHORIZATION":
        switch (status) {
          case "TRANSFER":
            return setActiveStep(1);
          case "DELIVER":
            return setActiveStep(2);
          case "PICKED_UP":
            return setActiveStep(3);
          default:
            return "Error AUTHORIZATION";
        }
      case "PROJECT_DEVELOP":
        return setActiveStep(4);
      case "REBELLION":
        switch (status) {
          case "BEGIN":
            return setActiveStep(5);
          case "DISPOSITION":
            return setActiveStep(6);
          default:
            return "Error REBELLION";
        }
      case "REVIEW":
        switch (status) {
          case "BEGIN":
            return setActiveStep(7);
          case "TRANSFER":
            return setActiveStep(8);
          case "CORRECTIONS":
            return setActiveStep(9);
          case "APROVED":
            return setActiveStep(10);
          default:
            return "Error REVIEW";
        }
      default:
        return "Error main";
    }
  };
  return { activeStep };
}

export { HandleCaseFileProgress };
