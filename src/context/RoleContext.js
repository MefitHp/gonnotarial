import React, { createContext, useState, useEffect } from "react";
// import { Auth } from "aws-amplify";

const RoleContext = createContext();
let { Provider, Consumer } = RoleContext;

function RoleProvider({ children }) {
  let [role, setRole] = useState(null);

  useEffect(() => {
    getRole();
  }, []);

  const getRole = async () => {
    //ROLS = CONTROL_TABLE | LAW_FIRM | ADMIN | ¿CLIENT?
    //Guarda en la variable currentRole el usuario autenticado y su data.
    // const currentRole = await Auth.currentSession();
    //Busca en el usuario actual su rol y lo settea
    // setRole(currentRole.idToken.payload["cognito:groups"][0]);
    // setRole("CONTROL_TABLE");
    // setRole("LAW_FIRM");
    setRole("ADMIN");
  };
  return <Provider value={{ role, setRole }}>{children}</Provider>;
}

export { RoleProvider, Consumer as RoleConsumer, RoleContext };
