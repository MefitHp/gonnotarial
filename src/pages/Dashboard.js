import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import CrearExpediente from "../components/Expedientes/index";
import DashboardLayout from "../components/Dashboard/DashboardLayout";
import ListarExpedientes from "../components/Expedientes/ListarExpedientes";
import DetalleExpediente from "../components/Expedientes/Detalle/DetalleExpediente";
import { RoleProvider } from "../context/RoleContext";

const Dashboard = ({ match }) => {
  return (
    <RoleProvider>
      <DashboardLayout>
        <Switch>
          <Route exact path={`${match.path}`} component={() => <p>hola</p>} />
          <Route
            path={`${match.path}/crear-expediente`}
            component={CrearExpediente}
          />
          <Route
            path={`${match.path}/listado-expedientes`}
            component={ListarExpedientes}
          />
          <Route
            path={`${match.path}/expediente/:SK`}
            component={DetalleExpediente}
          />
          <Redirect from="/" to="/dashboard" />
          <Redirect to="/dashboard" />
        </Switch>
      </DashboardLayout>
    </RoleProvider>
  );
};

export default Dashboard;
