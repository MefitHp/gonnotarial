import React, { useState } from "react";
import { Box, Paper, Typography, Button, TextField } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import ChipInput from "material-ui-chip-input";
import { Rings } from "svg-loaders-react";
import { useMutation } from "react-apollo-hooks";
import { ADD_CLIENT } from "./Graphql/Mutations";
import { useSnackbar } from "notistack";
import { GET_CLIENTS } from "./Graphql/Query";

const NuevoActor = props => {
  let initState = {
    alias: "",
    fullName: "",
    mailAddresses: []
  };
  let [newActor, setNewActor] = useState(initState);
  let [loading, setLoading] = useState(false);
  let [chipError, setChipError] = useState(false);
  const styles = useStyles();
  const addClient = useMutation(ADD_CLIENT, {
    refetchQueries: [{ query: GET_CLIENTS }]
  });
  const { enqueueSnackbar } = useSnackbar();

  const handleChange = e => {
    let { name, value } = e.target;
    if (name === "fileNumber") {
      if (value.match("^[0-9/]*$") === null) return;
    }
    setNewActor(prev => {
      return { ...prev, [name]: value };
    });
  };

  const handleSubmit = e => {
    e.preventDefault();
    setLoading(true);
    if (!newActor.mailAddresses.length) {
      setLoading(false);
      return setChipError(true);
    }

    addClient({
      variables: {
        args: JSON.stringify(newActor)
      }
    })
      .then(data => {
        setNewActor(initState);
        setLoading(false);
        enqueueSnackbar("Cliente agregado", {
          variant: "success"
        });
      })
      .catch(err => {
        console.log(err.message);
        setLoading(false);
        enqueueSnackbar("Hubo un error al crear la escritura", {
          variant: "error"
        });
      });
  };

  return (
    <Paper className={styles.Paper}>
      <Box className={styles.Box}>
        <Typography className={styles.Title} component="h2" variant="h6">
          Añadir cliente
        </Typography>
        <form id="new-actor" onSubmit={handleSubmit}>
          <TextField
            name="alias"
            disabled={loading}
            value={newActor.alias}
            onChange={handleChange}
            required
            label="Alias"
            fullWidth
            margin="dense"
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
          <TextField
            name="fullName"
            disabled={loading}
            value={newActor.fullName}
            onChange={handleChange}
            required
            label="Nombre completo"
            fullWidth
            margin="dense"
            variant="outlined"
            InputLabelProps={{
              shrink: true
            }}
          />
          <ChipInput
            error={chipError}
            disabled={loading}
            name="mailAddresses"
            helperText="Después de teclear un email, presione la tecla ENTER para añadirlo a la lista "
            value={newActor.mailAddresses}
            label="Email"
            variant="outlined"
            margin="dense"
            fullWidth
            InputLabelProps={{
              shrink: true
            }}
            onChange={chips =>
              setNewActor(prev => {
                return { ...prev, mailAddresses: chips };
              })
            }
            onDelete={(value, index) => {
              setNewActor(prev => {
                const { mailAddresses } = prev;
                mailAddresses.splice(index, 1);
                return {
                  ...prev,
                  mailAddresses
                };
              });
            }}
          />
        </form>
      </Box>
      <Button
        disabled={loading}
        type="submit"
        form="new-actor"
        style={{
          boxShadow: "none",
          borderRadius: "0px 0px 4px 4px",
          marginTop: 8
        }}
        variant="contained"
        color="primary"
        fullWidth
        size="small"
      >
        Agregar cliente
        {loading && <Rings />}
      </Button>
    </Paper>
  );
};

const useStyles = makeStyles(theme => ({
  Paper: {
    maxWidth: 512,
    padding: 0,
    marginTop: "1rem"
  },
  Box: {
    padding: `${theme.spacing(2)}px ${theme.spacing(1)}px`
  },
  Title: {
    fontSize: 14,
    fontWeight: 600,
    marginBottom: theme.spacing(1)
  }
}));
export default NuevoActor;
