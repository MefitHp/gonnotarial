import { Storage } from "aws-amplify";

export async function handleGetFile(params) {
  const { Key } = JSON.parse(params);
  Storage.get(Key, { customPrefix: { public: "" } })
    .then(file => {
      console.log(file);
      window.location.href = file;
    })
    .catch(err => console.log(err));
}

export async function handlePutFile(params, file) {
  const { Key } = params;
  Storage.put(Key, file);
  return new Promise((resolve, reject) => {
    Storage.put(Key, file)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}
