import styled from "@emotion/styled";
import { Box } from "@rebass/grid";

export const DetalleContainer = styled(Box)`
  margin: auto;
  max-width: 1024px;
`;
