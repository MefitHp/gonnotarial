import React from "react";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import { HandleCaseFileProgress } from "../../../context/StepContext";
import Created from "./Etapas/Begin/Created";
import { Flex, Box } from "@rebass/grid";
import Authorization from "./Etapas/Authorization/Authorization";
import Notify from "./Etapas/Project_Develop/Notify";
import Rebellion from "./Etapas/Rebellion/Rebellion";
import Disposition from "./Etapas/Rebellion/Disposition";
import Review from "./Etapas/Review/Review";
import Corrections from "./Etapas/Review/Corrections";
import Aproved from "./Etapas/Review/Aproved";

const ExpedienteBody = ({ caseFile, role }) => {
  const { activeStep } = HandleCaseFileProgress(caseFile);
  const { SK, notify, dispose, projectType, narration = "" } = caseFile;
  const classes = useStyles();
  const { pdfFile } = caseFile;

  console.log(activeStep);
  return (
    <Paper className={classes.Paper}>
      <MobileStepper
        variant="progress"
        steps={11}
        position="static"
        activeStep={activeStep}
        className={classes.root}
      />
      <Box px={4}>
        <Flex width="100%" flexWrap="wrap">
          {activeStep === 0 && (
            <Created pdfFile={pdfFile} role={role} SK={SK} />
          )}

          {(activeStep === 1 || activeStep === 2 || activeStep === 3) && (
            <Authorization activeStep={activeStep} role={role} SK={SK} />
          )}
          {activeStep === 4 && <Notify notify={notify} SK={SK} />}
          {activeStep === 5 && <Rebellion SK={SK} />}
          {activeStep === 6 && <Disposition SK={SK} dispose={dispose} />}
          {(activeStep === 7 || activeStep === 8) && (
            <Review
              SK={SK}
              projectType={projectType}
              narration={narration}
              activeStep={activeStep}
            />
          )}
          {activeStep === 9 && <Corrections SK={SK} />}
          {activeStep === 10 && <Aproved SK={SK} />}
        </Flex>
      </Box>
    </Paper>
  );
};

const useStyles = makeStyles(theme => ({
  Paper: {
    padding: 8,
    minHeight: "50vh"
  },
  root: {
    backgroundColor: "white",
    flexGrow: 1,
    margin: 8,
    display: "flex",
    justifyContent: "center"
  },
  button: {
    margin: "auto"
  },
  Button: {
    margin: "0px 8px"
  },
  leftIcon: {
    marginRight: theme.spacing(1)
  }
}));

export default ExpedienteBody;
