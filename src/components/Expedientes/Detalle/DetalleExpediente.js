import React, { useEffect, useState, useContext } from "react";
import { useQuery } from "react-apollo-hooks";
import { GET_CASE_FILE } from "../Graphql/Query";
import { DetalleContainer } from "./Detalle.styled";
import ExpedienteHeader from "./ExpedienteHeader";
import { Container } from "@material-ui/core";
import ExpedienteBody from "./ExpedienteBody";
import { RoleContext } from "../../../context/RoleContext";

const DetalleExpediente = props => {
  let { SK } = props.match.params;
  let [caseFile, setCaseFile] = useState({});
  let { role } = useContext(RoleContext);
  const { data: queryData, error, loading } = useQuery(GET_CASE_FILE, {
    variables: { SK },
    fetchPolicy: "cache-and-network"
  });
  useEffect(() => {
    const onCompleted = queryData => {
      const caseFile = (queryData && queryData.getCaseFile) || {};

      setCaseFile(caseFile);
    };
    const onError = error => {
      console.log(error);
    };
    if (onCompleted || onError) {
      if (onCompleted && !loading && !error) {
        onCompleted(queryData);
      } else if (onError && !loading && error) {
        onError(error);
      }
    }
  }, [loading, queryData, error]);

  if (loading) return <p>Loading</p>;

  return (
    <Container maxWidth="lg">
      <DetalleContainer>
        <ExpedienteHeader caseFile={caseFile} />
        <ExpedienteBody caseFile={caseFile} role={role} />
      </DetalleContainer>
    </Container>
  );
};

export default DetalleExpediente;
