import React, { useState } from "react";
import { Flex, Box, Text } from "rebass";
import Files from "react-files";
import { Button } from "@material-ui/core";
import { handlePutFile } from "../../../utils";
import ProgressButton from "../../../../common/ProgressButton";

const ReviewReceipt = ({ SK }) => {
  const [file, setFile] = useState([]);
  const [fileLoading, setFileLoading] = useState(false);
  return (
    <section
      style={{
        marginTop: 20,
        paddingTop: 20,
        width: "100%"
      }}
    >
      <Text as="p" fontWeight={350} fontSize={2}>
        Sube el archivo del acuse que muestra que entregaste la escritura en el
        juzgado. Esta acción enviará el acuse al cliente.
      </Text>
      <Flex width="100%" flexWrap="wrap" justifyContent="space-around">
        <Flex
          p={1}
          justifyContent="center"
          flexDirection="column"
          alignItems="center"
        >
          <Files
            className="files-dropzone"
            onChange={files => {
              console.log("SK", SK);
              setFileLoading(true);
              const [file] = files;
              const idFile = SK.slice(3);
              const path = `${idFile}/AcuseRevisionProyecto.${file.extension}`;
              const params = {
                Key: path
              };
              handlePutFile(params, file)
                .then(res => {
                  setFileLoading(false);
                  setFile(files);
                  return console.log(res);
                })
                .catch(err => {
                  console.error(err);
                  return setFileLoading(false);
                });
              // setFile(file)
            }}
            // onError={this.onFilesError}
            accepts={[".docx", ".pdf"]}
            clickable
          >
            <Button color="primary">
              {fileLoading ? (
                <div className="fa-1x">
                  <i className="fad fa-circle-notch fa-spin" />
                </div>
              ) : (
                "Seleccionar archivo"
              )}
            </Button>
          </Files>
          <p>
            {file && file.map(file => <span key={file.id}>{file.name}</span>)}
          </p>
        </Flex>
        <Box p={1} width={[1, 1 / 2, 1 / 3]}>
          <Flex style={{ height: "100%" }}>
            <ProgressButton
              SK={SK}
              icon="fad fa-arrow-circle-up"
              message="Se ha enviado un correo al cliente con el acuse adjunto"
              disabled={!file.length}
            >
              Subir acuse
            </ProgressButton>
          </Flex>
        </Box>
      </Flex>
    </section>
  );
};

export default ReviewReceipt;
