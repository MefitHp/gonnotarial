import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography
} from "@material-ui/core";
import { Box, Flex } from "rebass";
import { ExpandMore } from "@material-ui/icons";
import NarrarExpediente from "../../../../common/NarrarExpediente";
import DescargarArchivo from "../../../../common/DescargarArchivo";

const Promotion = ({ narration, SK }) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={classes.root}>
      <ExpansionPanel
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading}>
            Exhibir escritura sin expediente.
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Box width={[1, 3 / 4]}>
            <Typography>
              Da click para descargar el documento de exhibición de escritura
              sin devolver expediente al juzgado.
            </Typography>
          </Box>
          <Flex width={[1, 1 / 4]}>
            <DescargarArchivo
              variant="contained"
              color="primary"
              text="Descargar"
              SK={SK}
              fileName="Exhibir_sinDevolver.pdf"
            />
          </Flex>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <Box p={2} />
      <ExpansionPanel
        expanded={expanded === "panel2"}
        onChange={handleChange("panel2")}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.heading}>
            Exhibir escritura con expediente.
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <NarrarExpediente narration={narration} SK={SK} />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: 550
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  }
}));

export default Promotion;
