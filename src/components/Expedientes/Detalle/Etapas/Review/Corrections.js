import React from "react";
import { Flex, Text } from "rebass";
import ProgressButton from "../../../../common/ProgressButton";

const Corrections = ({ SK }) => {
  return (
    <Flex flexDirection="column" alignItems="center">
      <Text as="h3">¿Hay correcciones?</Text>
      <Text as="p" fontSize="1.5rem">
        Esta página sólo es para verificar si el proyecto necesita correcciones,
        presiona el botón sólo cuándo el proyecto no necesite más correciones y
        el proyecto esté aprobado.
      </Text>
      <Flex p={2} />
      <ProgressButton
        SK={SK}
        icon="fad fa-check-square"
        message="El cliente ha sido notificado de esta acción"
      >
        Proyecto Aprobado
      </ProgressButton>
    </Flex>
  );
};

export default Corrections;
