import React, { useState } from "react";
import { Flex, Heading, Text, Box } from "rebass";
import { Button } from "@material-ui/core";
import { useMutation } from "react-apollo-hooks";
import { GET_CASE_FILE } from "../../../Graphql/Query";
import { UPDATE_CASE_FILE } from "../../../Graphql/Mutations";
import Promotion from "./Promotion";
import ProgressButton from "../../../../common/ProgressButton";
import ReviewReceipt from "./ReviewReceipt";
const Review = ({ projectType, SK, narration, activeStep }) => {
  const updateCF = useMutation(UPDATE_CASE_FILE);
  const [loading, setIsLoading] = useState(false);

  const updateCaseFile = newType => {
    setIsLoading(true);
    const args = JSON.stringify({ SK, projectType: newType });

    return updateCF({
      variables: { args },
      refetchQueries: [{ query: GET_CASE_FILE, variables: { SK } }]
    })
      .then(data => {
        setIsLoading(false);
        return console.log("PROJECT TYPE DATA", data);
      })
      .catch(err => {
        setIsLoading(false);
        return console.error(err.message);
      });
  };

  return (
    <Flex flexDirection="column" width="100%">
      {projectType === "not-selected" ? (
        <>
          <Box pb={3}>
            <Heading fontSize={[2, 3]} textAlign="center">
              Revisión de proyecto
            </Heading>
            <Text as="p" fontSize={[1, 2]} textAlign="center">
              Seleccione el tipo de proyecto
            </Text>
          </Box>
          <Flex width="100%" justifyContent="space-around">
            <Button
              variant="outlined"
              onClick={() => updateCaseFile("promotion")}
            >
              Promoción
            </Button>
            <Button
              variant="outlined"
              onClick={() => updateCaseFile("economic")}
            >
              Económica
            </Button>
          </Flex>
          {loading && <p>Espere un momento..</p>}
        </>
      ) : (
        <>
          <Promotion narration={narration} SK={SK} />
          {activeStep === 7 && (
            <Flex
              pt={3}
              justifyContent="center"
              alignItems="center"
              flexDirection="column"
            >
              <Text as="p">
                Presiona el botón para confirmar que has colectado el escrito
                para trasladarlo al juzgado.
              </Text>
              <Box p={2} />
              <ProgressButton icon="fad fa-hand-holding-box" SK={SK}>
                Colectado
              </ProgressButton>
            </Flex>
          )}
        </>
      )}
      {activeStep === 8 && <ReviewReceipt SK={SK} />}
    </Flex>
  );
};

export default Review;
