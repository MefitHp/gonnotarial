import React, { useState } from "react";
import { Box, Flex, Text, Heading } from "rebass";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useMutation } from "react-apollo-hooks";
import { UPDATE_CASE_FILE } from "../../../Graphql/Mutations";
import { GET_CASE_FILE } from "../../../Graphql/Query";
import DescargarArchivo from "../../../../common/DescargarArchivo";
import NotifyReceipt from "./NotifyReceipt";

const Notify = ({ SK, notify }) => {
  const [file, setFile] = useState([]);
  const [fileLoading, setFileLoading] = useState(false);
  const [notification, setNotification] = useState(notify);
  const classes = useStyles();
  const updateCF = useMutation(UPDATE_CASE_FILE);

  console.log(notify);
  const notificationDictionary = notify => {
    switch (notify) {
      case "CDMX":
        return "CDMX";
      case "foreign":
        return "Foraneo";
      case "judge":
        return "Juzgado";
      default:
        return notify;
    }
  };
  const handleNofitication = selectedValue => {
    const args = JSON.stringify({ SK, notify: selectedValue });
    setNotification(selectedValue);
    if (selectedValue !== "select-notification") {
      updateCF({
        variables: { args },
        refetchQueries: [{ query: GET_CASE_FILE, variables: { SK } }]
      });
    }
  };
  return (
    <>
      <Box p={1} width={1}>
        <Heading as="h3" my={1} fontSize={[3, 4]} textAlign="center">
          Elaboración del projecto
        </Heading>
      </Box>
      {notification === "not-selected" && (
        <>
          <Box p={1} width={1}>
            <Text my={1} textAlign="center" fontSize={[3, 4]}>
              ¿Se necesita notificar?
            </Text>
            <Flex justifyContent="center">
              <NotifyButton
                onClick={() => handleNofitication("select-notification")}
              >
                Si
              </NotifyButton>
              <Button
                variant="outlined"
                className={classes.Button}
                onClick={() => handleNofitication("dont-notify")}
              >
                No
              </Button>
            </Flex>
          </Box>
        </>
      )}
      {notification === "select-notification" && (
        <>
          <Box p={1} width={1}>
            <Text my={1} textAlign="center" fontSize={[3, 4]}>
              ¿Quién notifica?
            </Text>
            <Flex flexWrap="wrap">
              <Box p={1} width={[1, 1 / 2]}>
                <Text my={1} textAlign="center" fontSize={[1, 2]}>
                  Juzgado
                </Text>
                <Flex justifyContent="center">
                  <NotifyButton onClick={() => handleNofitication("judge")}>
                    Juez
                  </NotifyButton>
                </Flex>
              </Box>
              <Box p={1} width={[1, 1 / 2]}>
                <Text my={1} textAlign="center" fontSize={[1, 2]}>
                  Notaría
                </Text>
                <Flex justifyContent="center">
                  <NotifyButton onClick={() => handleNofitication("CDMX")}>
                    CDMX
                  </NotifyButton>
                  <NotifyButton onClick={() => handleNofitication("foreign")}>
                    Foraneo
                  </NotifyButton>
                </Flex>
              </Box>
            </Flex>
          </Box>
        </>
      )}
      <Flex width="100%" flexWrap="wrap">
        {notify !== ("not-selected" || "dont-notify") && (
          <>
            <Flex
              width={[1, 1 / 2]}
              justifyContent="center"
              alignItems="center"
            >
              <p>
                Se ha seleccionado notificación por medio{" "}
                {notificationDictionary(notify)}
              </p>
            </Flex>
            <Flex
              width={[1, 1 / 2]}
              justifyContent="center"
              alignItems="center"
            >
              <DescargarArchivo
                SK={SK}
                text="Descargar doc. de notificación"
                fileName="Notificacion.pdf"
              />
            </Flex>
            <NotifyReceipt
              SK={SK}
              file={file}
              setFile={setFile}
              fileLoading={fileLoading}
              setFileLoading={setFileLoading}
            />
          </>
        )}
        {notify === "dont-notify" && (
          <Text my={1} textAlign="center" fontSize={[3, 4]} as="p">
            No se necesita notificar.
          </Text>
        )}
        {/* <ProgressButton SK={SK} icon="fas fa-hand-holding-box">
                    Confirmar
                  </ProgressButton>
              */}
      </Flex>
    </>
  );
};

const NotifyButton = ({ children, ...rest }) => {
  const classes = useStyles();
  return (
    <Button
      variant="outlined"
      className={classes.Button}
      color="primary"
      {...rest}
    >
      {children}
    </Button>
  );
};

const useStyles = makeStyles(theme => ({
  Button: {
    margin: "0px 8px"
  }
}));

export default Notify;
