import React from "react";
import { Flex, Box, Text } from "rebass";
import { handlePutFile } from "../../../utils";
import Files from "react-files";
import ProgressButton from "../../../../common/ProgressButton";
import { Button } from "@material-ui/core";

const NotifyReceipt = ({ file, setFile, fileLoading, setFileLoading, SK }) => {
  return (
    <section
      style={{
        marginTop: 20,
        paddingTop: 20,
        borderTop: "1px solid rgba(0,0,0,0.3",
        width: "100%"
      }}
    >
      <Text as="p" fontWeight={350} fontSize={2}>
        Sube el archivo del acuse que muestra que el demandado no se presentó a
        firmar. <br /> El boton <b>SUBIR ACUSE </b>, enviará al cliente un
        correo preguntando si se deben pedir certificaciones y continuará con el
        proceso.
      </Text>
      <Flex width="100%" flexWrap="wrap" justifyContent="space-around">
        <Flex
          p={1}
          justifyContent="center"
          flexDirection="column"
          alignItems="center"
        >
          <Files
            className="files-dropzone"
            onChange={files => {
              console.log("SK", SK);
              setFileLoading(true);
              const [file] = files;
              const idFile = SK.slice(3);
              const path = `${idFile}/AcuseNotificacion.${file.extension}`;
              const params = {
                Key: path
              };
              handlePutFile(params, file)
                .then(res => {
                  setFileLoading(false);
                  setFile(files);
                  return console.log(res);
                })
                .catch(err => {
                  console.error(err);
                  return setFileLoading(false);
                });
              // setFile(file)
            }}
            // onError={this.onFilesError}
            accepts={[".docx", ".pdf"]}
            clickable
          >
            <Button color="primary">
              {fileLoading ? (
                <div className="fa-1x">
                  <i className="fad fa-circle-notch fa-spin" />
                </div>
              ) : (
                "Seleccionar archivo"
              )}
            </Button>
          </Files>
          <p>
            {file && file.map(file => <span key={file.id}>{file.name}</span>)}
          </p>
        </Flex>
        <Box p={1} width={[1, 1 / 2, 1 / 3]}>
          <Flex style={{ height: "100%" }}>
            <ProgressButton
              SK={SK}
              icon="fad fa-arrow-circle-up"
              message="Se ha enviado un correo al cliente con el acuse adjunto"
              disabled={!file.length}
            >
              Subir acuse
            </ProgressButton>
          </Flex>
        </Box>
      </Flex>
    </section>
  );
};

export default NotifyReceipt;
