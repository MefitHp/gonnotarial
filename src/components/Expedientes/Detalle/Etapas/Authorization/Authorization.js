import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Stepper, Step, StepLabel, StepContent } from "@material-ui/core";
import { Flex } from "rebass";

import Transfer from "./Status/Transfer";
import Deliver from "./Status/Deliver";
import PickedUp from "./Status/PickedUp";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  }
}));

function getSteps() {
  return [
    "Trasladar escrito al Juzgado",
    "Escrito entregado al juzgado",
    "Confirmar recuperación de expediente"
  ];
}

const Authorization = ({ role, SK, activeStep }) => {
  const classes = useStyles();
  const steps = getSteps();
  const [file, setFile] = useState([]);

  const getStepContent = (step, SK) => {
    console.log(role);
    switch (step) {
      case 1:
        return <Transfer role={role} SK={SK} />;
      case 2:
        return <Deliver role={role} SK={SK} file={file} setFile={setFile} />;
      case 3:
        return <PickedUp role={role} SK={SK} />;
      default:
        return "Unknown step";
    }
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep - 1} orientation="vertical">
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              <Flex mb={2} width="100%" flexWrap="wrap">
                {getStepContent(activeStep, SK)}
              </Flex>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {/* {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
        </Paper>
      )} */}
    </div>
  );
};

export default Authorization;
