import React from "react";
import { Text, Box, Flex } from "rebass";
import ProgressButton from "../../../../../common/ProgressButton";

const Transfer = ({ role, SK }) => {
  return (
    <>
      {role === "LAW_FIRM" || role === "ADMIN" ? (
        <>
          <Text my={1} fontSize={1}>
            Presiona en "Colectado" cuándo hayas recibido el escrito
          </Text>
          <Box p={1} width={[1, 1 / 2, 1 / 3]}>
            <Flex style={{ height: "100%" }}>
              <ProgressButton icon="fas fa-hand-holding-box" SK={SK}>
                Colectado
              </ProgressButton>
            </Flex>
          </Box>
        </>
      ) : (
        <Text my={1} fontSize={2}>
          El documento se encuentra en traslado al juzgado, por el momento
          tienes que preocuparte por nada.
        </Text>
      )}
    </>
  );
};

export default Transfer;
