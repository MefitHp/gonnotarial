import React, { useState } from "react";
import { Button } from "@material-ui/core";
import { Flex, Box, Text } from "rebass";
import Files from "react-files";
import ProgressButton from "../../../../../common/ProgressButton";
import { handlePutFile } from "../../../../utils";

const Deliver = ({ role, SK, file, setFile }) => {
  const [fileLoading, setFileLoading] = useState(false);

  return (
    <>
      {role === "LAW_FIRM" || role === "ADMIN" ? (
        <>
          <Box p={1} width={[1, 1 / 2, 2 / 3]}>
            <Text my={1} fontSize={1}>
              Selecciona un archivo y a continuación da click en subir acuse.
            </Text>
          </Box>
          <Flex
            p={1}
            width={[1, 1 / 3]}
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
          >
            <Files
              className="files-dropzone"
              onChange={files => {
                console.log("SK", SK);
                setFileLoading(true);
                const [file] = files;
                const idFile = SK.slice(3);
                const path = `${idFile}/Acuse.${file.extension}`;
                const params = {
                  Key: path
                };
                handlePutFile(params, file)
                  .then(res => {
                    setFileLoading(false);
                    setFile(files);
                    return console.log(res);
                  })
                  .catch(err => {
                    console.error(err);
                    return setFileLoading(false);
                  });
                // setFile(file)
              }}
              // onError={this.onFilesError}
              accepts={[".docx", ".pdf"]}
              clickable
            >
              <Button color="primary">
                {fileLoading ? (
                  <div className="fa-1x">
                    <i className="fad fa-circle-notch fa-spin" />
                  </div>
                ) : (
                  "Seleccionar archivo"
                )}
              </Button>
            </Files>
            <p>
              {file && file.map(file => <span key={file.id}>{file.name}</span>)}
            </p>
          </Flex>
          <Box p={1} width={[1, 1 / 2, 1 / 3]}>
            <Flex style={{ height: "100%" }}>
              <ProgressButton
                SK={SK}
                icon="fad fa-arrow-circle-up"
                message="Se ha enviado un correo al cliente con el acuse adjunto"
                disabled={!file.length}
              >
                Subir acuse
              </ProgressButton>
            </Flex>
          </Box>
        </>
      ) : (
        <Text my={1} fontSize={2}>
          El documento se encuentra en traslado al juzgado, por el momento
          tienes que preocuparte por nada.
        </Text>
      )}
    </>
  );
};

export default Deliver;
