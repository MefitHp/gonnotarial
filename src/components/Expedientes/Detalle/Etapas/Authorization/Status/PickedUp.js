import React from "react";
import { Box, Heading, Text, Flex } from "rebass";
import ProgressButton from "../../../../../common/ProgressButton";

const PickedUp = ({ role, SK }) => {
  return (
    <>
      {role === "LAW_FIRM" || role === "ADMIN" ? (
        <>
          <Box p={1} width={[1, 1 / 2, 2 / 3]}>
            <Heading as="h3" my={1} fontSize={[3, 4]}>
              En espera de confirmación
            </Heading>
            <Text my={1} fontSize={1}>
              Confirma que recogiste el expediente dando click en el botón
              confirmar. Al momento que des click, se enviará un correo
              electrónico al cliente notificando esta acción.
            </Text>
          </Box>
          <Box p={1} width={[1, 1 / 2, 1 / 3]}>
            <Flex style={{ height: "100%" }}>
              <ProgressButton
                SK={SK}
                icon="fas fa-hand-holding-box"
                message="Se ha enviado un correo al cliente notificando esta acción"
              >
                Confirmar
              </ProgressButton>
            </Flex>
          </Box>
        </>
      ) : (
        <Text my={1} fontSize={2}>
          El documento se encuentra en traslado al juzgado, por el momento
          tienes que preocuparte por nada.
        </Text>
      )}
    </>
  );
};

export default PickedUp;
