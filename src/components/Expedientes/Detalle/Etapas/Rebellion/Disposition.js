import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { Flex, Heading, Text } from "rebass";
import { useMutation } from "react-apollo-hooks";
import {
  UPDATE_CASE_FILE,
  CASE_FILE_PROGRESS
} from "../../../Graphql/Mutations";
import { useSnackbar } from "notistack";
import { GET_CASE_FILE } from "../../../Graphql/Query";

const Disposition = ({ SK, dispose }) => {
  const classes = useStyles();
  const [loading, setIsLoading] = useState(false);
  const updateCaseFile = useMutation(UPDATE_CASE_FILE);
  const handleCaseFile = useMutation(CASE_FILE_PROGRESS);
  const { enqueueSnackbar } = useSnackbar();
  console.log("DISPOSE", dispose);
  const shouldDispose = value => {
    setIsLoading(true);
    const args = JSON.stringify({ SK, dispose: value });
    return updateCaseFile({
      variables: { args }
    })
      .then(data => {
        return handleCaseFile({
          variables: { SK },
          refetchQueries: [{ query: GET_CASE_FILE, variables: { SK } }]
        })
          .then(data => {
            setIsLoading(false);
            return enqueueSnackbar("Se pedirá disposición del expediente", {
              variant: "info"
            });
          })
          .catch(err => {
            console.error(err.message);
            return setIsLoading(false);
          });
      })
      .catch(err => {
        console.log(err.message);
        return setIsLoading(false);
      });
  };
  return (
    <Flex flexDirection="column" width="100%">
      {dispose === "not-selected" ? (
        <>
          <Heading fontSize={[2, 3]} textAlign="center">
            ¿Pedir que se ponga a disposición el expediente?
          </Heading>
          <Text fontSize={[1, 2]}>
            Al pedir la disposición del expediente, se le enviará un email al
            cliente notificando esta acción.
          </Text>
          <Flex justifyContent="center" width="100%" pt={4}>
            <Button
              disabled={loading}
              onClick={() => shouldDispose("not-dispose")}
              className={classes.Button}
              variant="contained"
              color="secondary"
            >
              No pedir
            </Button>
            <Button
              disabled={loading}
              className={classes.Button}
              onClick={() => shouldDispose("dispose")}
              variant="contained"
              color="primary"
            >
              Pedir disposición
            </Button>
          </Flex>
        </>
      ) : null}
      <p style={{ width: "100%", textAlign: "center" }}>
        {loading ? "Espere un momento..." : ""}
      </p>
    </Flex>
  );
};

const useStyles = makeStyles(theme => ({
  Button: {
    padding: `${theme.spacing(1)}px ${theme.spacing(2)}px`,
    margin: `0px ${theme.spacing(1)}px`,
    minWidth: 180
  }
}));

export default Disposition;
