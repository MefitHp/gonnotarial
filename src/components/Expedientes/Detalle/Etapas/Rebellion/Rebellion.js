import React from "react";
import { Heading, Flex, Box, Text } from "rebass";
import ProgressButton from "../../../../common/ProgressButton";
const Rebellion = ({ SK }) => {
  return (
    <Flex flexDirection="column" width="100%">
      <Heading fontSize={[3, 4, 5]} textAlign="center">
        Rebeldía
      </Heading>
      <Text fontSize={[1, 2]}>
        Da click en el siguiente botón para confirmar que el juez acusó la
        rebeldía y firmó por el demandado. <br />
        <b>
          PD: La siguiente acción continuará con el proceso y notificará esta
          acción al cliente.
        </b>
      </Text>
      <Flex pt={3}>
        <ProgressButton
          SK={SK}
          icon="fad fa-arrow-circle-up"
          message="Se ha enviado un correo al cliente notificando esta acción"
        >
          Confirmar rebeldía
        </ProgressButton>
      </Flex>
    </Flex>
  );
};

export default Rebellion;
