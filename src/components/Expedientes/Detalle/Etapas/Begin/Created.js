import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Flex, Box } from "@rebass/grid";
import { Text, Heading } from "rebass";
import { Button, Divider } from "@material-ui/core";
import { handleGetFile } from "../../../utils";
import ProgressButton from "../../../../common/ProgressButton";

const Created = ({ pdfFile, role, SK }) => {
  const classes = useStyles();
  return (
    <>
      <Flex width="100%" flexWrap="wrap">
        {role === "LAW_FIRM" && (
          <>
            <Box p={1}>
              <Heading as="h3" my={1} fontSize={[3, 4]}>
                Entrega pendiente.
              </Heading>
              <Text my={1} fontSize={2}>
                La carta de autorización se encuentra pendiente de entrega al
                notario, mientras tanto, puedes descargar una a copia del
                documento a continuación.
              </Text>
              <Flex style={{ paddingTop: 20 }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={() => handleGetFile(pdfFile)}
                >
                  Descargar
                  <i
                    className="fas fa-file-download"
                    style={{ paddingLeft: 8 }}
                  />
                </Button>
              </Flex>
            </Box>
          </>
        )}

        {(role === "CONTROL_TABLE" || role === "ADMIN") && (
          <>
            <Box p={1} width={[1, 1 / 2, 2 / 3]}>
              <Heading as="h3" my={1} fontSize={[3, 4]}>
                Carta de autorización.
              </Heading>
              <Text my={1} fontSize={1}>
                La carta de autorización se ha generado correctamente, da click
                en el botón para descargarla.
              </Text>
            </Box>
            <Box p={1} width={[1, 1 / 2, 1 / 3]}>
              <Flex style={{ height: "100%" }}>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={() => handleGetFile(pdfFile)}
                >
                  Descargar
                  <i
                    className="fas fa-file-download"
                    style={{ paddingLeft: 8 }}
                  />
                </Button>
              </Flex>
            </Box>
            <Divider variant="middle" style={{ margin: 16 }} />
            <Flex width="100%" flexWrap="wrap">
              <Box p={1} width={[1, 1 / 2, 2 / 3]}>
                <Heading as="h3" my={1} fontSize={[3, 4]}>
                  Firma con notario.
                </Heading>
                <Text my={1} fontSize={1}>
                  Da click en el botón para confirmar que este documento se
                  envió a firma con el notario.
                </Text>
              </Box>
              <Box p={1} width={[1, 1 / 2, 1 / 3]}>
                <Flex style={{ height: "100%" }}>
                  <ProgressButton icon="fas fa-pen-nib" SK={SK}>
                    Confirmar
                  </ProgressButton>
                </Flex>
              </Box>
            </Flex>
          </>
        )}
      </Flex>
    </>
  );
};

const useStyles = makeStyles(theme => ({
  button: {
    margin: "auto"
  }
}));

export default Created;
