import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  // ExpansionPanelActions,
  Typography
} from "@material-ui/core";
import { Box, Flex } from "@rebass/grid";
import { ExpandMore } from "@material-ui/icons";
import { handleFileStatus } from "../../../utils/utils";

const ExpedienteHeader = ({ caseFile }) => {
  console.log(caseFile);
  const classes = useStyles();
  const { fileNumber, defendant, phase, court, actorData, status } = caseFile;
  //Si hay data del actor, parsea la información y extraé el fullName, si no, devuelve un string vacío.
  const actorFullName = (actorData && JSON.parse(actorData).fullName) || "";
  return (
    <div className={classes.root}>
      <ExpansionPanel defaultExpanded>
        <ExpansionPanelSummary
          expandIcon={<ExpandMore />}
          aria-controls="panel-content"
          id="panel-header"
        >
          <div className={classes.column}>
            <Typography className={classes.heading}>
              <i className="fas fa-archive" style={{ paddingRight: 8 }} />{" "}
              EXPEDIENTE {fileNumber}
            </Typography>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Flex flexWrap="wrap" width="100%">
            <Box width={[1, 1 / 2, 2 / 3]} py={1}>
              <Flex>
                <span className={classes.info}>
                  <span style={{ fontSize: 20, fontWeight: 600 }}>
                    <b>Demandado:</b> {defendant}
                  </span>{" "}
                  <br />
                  <b>Cliente:</b> {actorFullName} <br />
                  <b>Juzgado: </b> {court} <br />
                </span>
              </Flex>
            </Box>
            <Box className={classes.helper} width={[1, 1 / 2, 1 / 3]}>
              <Typography
                variant="caption"
                style={{
                  paddingBottom: 10,
                  marginBottom: "1px solid rgba(0,0,0,.3)"
                }}
              >
                <p
                  style={{
                    textAlign: "center"
                  }}
                >
                  Fase del expediente
                </p>
                {handleFileStatus(phase)}
              </Typography>
              <Typography variant="caption">
                <p style={{ textAlign: "center" }}>Estado</p>
                {handleFileStatus(status)}
              </Typography>
            </Box>
          </Flex>
        </ExpansionPanelDetails>
        {/* <Divider />
        <ExpansionPanelActions>
          <Button size="small">Cancel</Button>
          <Button size="small" color="primary">
            Save
          </Button>
        </ExpansionPanelActions> */}
      </ExpansionPanel>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginBottom: "20px"
  },
  info: {
    [theme.breakpoints.only("xs")]: {
      margin: "auto"
    }
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    fontWeight: 500,
    color: "rgba(0,0,0,.45)"
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  icon: {
    verticalAlign: "bottom",
    height: 20,
    width: 20
  },
  details: {
    alignItems: "center"
  },
  column: {
    border: "1px solid palevioletrose"
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 2),
    [theme.breakpoints.only("xs")]: {
      borderLeft: "none",
      borderTop: `2px solid ${theme.palette.divider}`
    }
  }
}));

export default ExpedienteHeader;
