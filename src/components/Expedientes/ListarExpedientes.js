import React from 'react'
import ConsultarExpedientes from './ConsultarExpedientes';

const ListarExpedientes = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center'}}>
      <ConsultarExpedientes />
    </div>
  )
}

export default ListarExpedientes
