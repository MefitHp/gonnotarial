import React from "react";
import { Flex, Box } from "@rebass/grid";
import NuevoExpediente from "./NuevoExpediente";
import ConsultarExpedientes from "./ConsultarExpedientes";
import NuevoActor from "./NuevoActor";

const index = () => {
  return (
    <Flex style={{ flexWrap: "wrap" }}>
      <Box width={[1, 1, 3 / 8]} p={[4, 4, 3]}>
        <NuevoExpediente />
        <NuevoActor />
      </Box>
      <Box width={[1, 1, 5 / 8]} p={[4, 4, 3]}>
        <ConsultarExpedientes />
      </Box>
    </Flex>
  );
};

export default index;
