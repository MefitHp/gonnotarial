import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import MaterialTable from "material-table";
import { useQuery } from "react-apollo-hooks";
import { GET_CASE_FILES } from "./Graphql/Query";
import { handleTableColumns } from "../../utils/utils";

const ConsultarExpedientes = () => {
  const [tableData, setTableData] = useState([]);
  const { data: queryData, error, loading } = useQuery(GET_CASE_FILES, {
    fetchPolicy: "cache-and-network"
  });
  useEffect(() => {
    const onCompleted = queryData => {
      const items =
        (queryData && queryData.getCaseFiles && queryData.getCaseFiles.items) ||
        [];
      const filteredItems = items.filter(item => !item.isDeleted);
      return setTableData(filteredItems);
    };
    const onError = error => {
      /* magic */ console.log(error);
    };
    if (onCompleted || onError) {
      if (onCompleted && !loading && !error) {
        onCompleted(queryData);
      } else if (onError && !loading && error) {
        onError(error);
      }
    }
  }, [loading, queryData, error]);

  return (
    <>
      <MaterialTable
        style={{ maxWidth: 1024 }}
        title="Listado de expedientes"
        isLoading={loading}
        columns={handleTableColumns("LAW_FIRM")}
        data={tableData}
      />
    </>
  );
};

export default withRouter(ConsultarExpedientes);
