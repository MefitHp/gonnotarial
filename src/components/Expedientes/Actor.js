import React, { useEffect, useState, useRef } from "react";
import { useQuery } from "react-apollo-hooks";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  OutlinedInput,
  Box
} from "@material-ui/core";
import { GET_CLIENTS } from "./Graphql/Query";

const Actor = ({ ...rest }) => {
  let [clients, setClients] = useState([]);
  let [labelWidth, setLabelWidth] = useState(0);
  let inputLabel = useRef(null);
  let { loading, error, data: queryData } = useQuery(GET_CLIENTS, {
    fetchPolicy: "cache-and-network",
    errorPolicy: "ignore"
  });
  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
    const onCompleted = queryData => {
      const clients = (queryData && queryData.getClients) || [];
      return setClients(clients);
    };
    const onError = error => {
      console.log(error);
    };
    if (onCompleted || onError) {
      if (onCompleted && !loading && !error) {
        onCompleted(queryData);
      } else if (onError && !loading && error) {
        onError(error);
      }
    }
  }, [loading, error, queryData]);

  return (
    <FormControl
      variant="outlined"
      required
      fullWidth
      style={{ margin: "8px 0" }}
    >
      <InputLabel ref={inputLabel}>Cliente</InputLabel>
      <Select {...rest} input={<OutlinedInput labelWidth={labelWidth} />}>
        <MenuItem value="null">Seleccione una opción..</MenuItem>
        {clients.map(client => (
          <MenuItem key={client.SK} value={client.SK}>
            <Box display="flex" flexDirection="column">
              <Box
                component="p"
                fontSize={16}
                fontWeight={500}
                style={{ margin: 0, marginBottom: 5 }}
              >
                {client.alias}
              </Box>
              <Box fontSize={10}>{client.fullName.toUpperCase()}</Box>
            </Box>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default Actor;
