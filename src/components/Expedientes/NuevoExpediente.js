import React, { useState, useRef, useEffect } from "react";
import {
  Paper,
  Typography,
  TextField,
  Box,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  OutlinedInput
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useMutation } from "react-apollo-hooks";
import { CREATE_CASE_FILE } from "./Graphql/Mutations";
import { Rings } from "svg-loaders-react";
import { useSnackbar } from "notistack";
import moment from "moment";
import { GET_CASE_FILES } from "./Graphql/Query";
import Actor from "./Actor";
const NuevoExpediente = () => {
  let initState = {
    notary: "GONZALO M. ORTIZ BLANCO",
    actorId: "null",
    defendant: "",
    court: "",
    fileNumber: "",
    data: "",
    isDeleted: false
  };
  let [labelWidth, setLabelWidth] = useState(0);
  let [escritura, setEscritura] = useState(initState);
  let [loading, setLoading] = useState(false);
  let [actorErr, setActorErr] = useState(false);
  const styles = useStyles();
  const inputLabel = useRef(null);
  const crearEscritura = useMutation(CREATE_CASE_FILE);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const handleChange = e => {
    let { name, value } = e.target;
    if (name === "fileNumber") {
      if (value.match("^[0-9/]*$") === null) return;
    }
    if (name === "actorId") {
      setActorErr(false);
    }
    setEscritura(prev => {
      return { ...prev, [name]: value };
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    if (escritura.actorId === "null") {
      setActorErr(true);
      return;
    }
    setLoading(true);
    const args = {
      ...escritura,
      data: `CF#${moment().unix()}`
    };
    crearEscritura({
      variables: { args: JSON.stringify(args) },
      refetchQueries: [{ query: GET_CASE_FILES }]
    })
      .then(data => {
        setEscritura(initState);
        setLoading(false);
        enqueueSnackbar("Escrito creado correctamente", {
          variant: "success"
        });
        enqueueSnackbar(
          "Se ha enviado una copia por correo a mesa de control",
          {
            variant: "info"
          }
        );
      })
      .catch(err => {
        console.log(err);
        setLoading(false);
        enqueueSnackbar("Hubo un error al crear la escritura", {
          variant: "error"
        });
      });
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Paper className={styles.Paper}>
        <Box className={styles.Box}>
          <Typography className={styles.Title} component="h2" variant="h6">
            Crear expediente
          </Typography>
          <form onSubmit={onSubmit} id="rubro-form">
            <FormControl
              variant="outlined"
              required
              style={{ marginBottom: 8 }}
              fullWidth
            >
              <InputLabel ref={inputLabel}>Notario</InputLabel>
              <Select
                disabled={loading}
                name="notary"
                margin="dense"
                onChange={handleChange}
                value={escritura.notary}
                placeholder="Notario"
                input={<OutlinedInput labelWidth={labelWidth} />}
              >
                <MenuItem value="GONZALO M. ORTIZ BLANCO">
                  Gonzalo M. Ortiz Blanco
                </MenuItem>
                <MenuItem value="LUIS RICARDO DUARTE GUERRA">
                  Luis Ricardo Duarte Guerra
                </MenuItem>
              </Select>
            </FormControl>
            <Actor
              error={actorErr}
              disabled={loading}
              margin="dense"
              name="actorId"
              onChange={handleChange}
              placeholder="Cliente"
              value={escritura.actorId}
            />
            <TextField
              name="defendant"
              disabled={loading}
              value={escritura.defendant}
              onChange={handleChange}
              required
              label="Nombre del demandado"
              placeholder="Ej. Martín Rivas"
              fullWidth
              margin="dense"
              variant="outlined"
              InputLabelProps={{
                shrink: true
              }}
            />

            <TextField
              required
              disabled={loading}
              name="court"
              value={escritura.court}
              onChange={handleChange}
              label="Juzgado"
              InputLabelProps={{
                shrink: true
              }}
              fullWidth
              margin="dense"
              variant="outlined"
              style={{ marginRight: 16 }}
            />
            <TextField
              required
              disabled={loading}
              value={escritura.fileNumber}
              name="fileNumber"
              onChange={handleChange}
              label="No. de Expediente"
              InputLabelProps={{
                shrink: true
              }}
              fullWidth
              margin="dense"
              variant="outlined"
            />
          </form>
        </Box>
        <Button
          disabled={loading}
          type="submit"
          form="rubro-form"
          style={{ boxShadow: "none", borderRadius: "0px 0px 4px 4px" }}
          variant="contained"
          color="primary"
          fullWidth
          size="large"
        >
          Crear expediente
          {loading && <Rings />}
        </Button>
      </Paper>
    </div>
  );
};
const useStyles = makeStyles(theme => ({
  Paper: {
    maxWidth: 512,
    padding: 0
  },
  Box: {
    padding: `${theme.spacing(3)}px ${theme.spacing(2)}px`
  },
  Title: {
    fontSize: 24,
    fontWeight: 600,
    marginBottom: theme.spacing(1)
  }
}));
export default NuevoExpediente;
