import gql from "graphql-tag";

export const CREATE_CASE_FILE = gql`
  mutation createCaseFile($args: AWSJSON) {
    createCaseFile(args: $args) {
      PK
      SK
    }
  }
`;

export const ADD_CLIENT = gql`
  mutation addClient($args: AWSJSON) {
    addClient(args: $args) {
      SK
    }
  }
`;

export const UPDATE_CASE_FILE = gql`
  mutation updateCaseFile($args: AWSJSON) {
    updateCaseFile(args: $args)
  }
`;

export const CASE_FILE_PROGRESS = gql`
  mutation caseFileProgress($SK: ID!) {
    caseFileProgress(SK: $SK)
  }
`;
