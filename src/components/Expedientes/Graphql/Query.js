import gql from "graphql-tag";

export const GET_CASE_FILES = gql`
  query getCaseFiles($nextToken: String, $limit: Int) {
    getCaseFiles(nextToken: $nextToken, limit: $limit) {
      items {
        SK
        data
        actorId
        actorData
        defendant
        court
        fileNumber
        status
        pdfFile
        isDeleted
        phase
        notify
        projectType
        narration
      }
      nextToken
      total
    }
  }
`;

export const GET_CASE_FILE = gql`
  query getCaseFile($SK: ID!) {
    getCaseFile(SK: $SK) {
      SK
      data
      actorId
      isDeleted
      actorData
      defendant
      fileNumber
      court
      status
      notary
      pdfFile
      phase
      notify
      dispose
      projectType
      narration
    }
  }
`;

export const GET_CLIENTS = gql`
  query getClients {
    getClients {
      PK
      SK
      alias
      fullName
      mailAddresses
    }
  }
`;
