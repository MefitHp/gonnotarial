export function handleLoginErrors(err) {
  switch (err.code) {
    case "NotAuthorizedException":
      return [
        "Nombre de usuario o contraseña incorrecto.",
        { variant: "error" }
      ];
    case "UserNotFoundException":
      return ["El usuario ingresado no existe", { variant: "warning" }];
    default:
      console.log(err);
      return ["Un error desconocido ha ocurrido", { variant: "error" }];
  }
}
