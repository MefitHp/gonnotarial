import React from "react";
import { Link } from "react-router-dom";
import { Button } from "@material-ui/core";

export const StyledLink = ({ to, children, ...rest }) => {
  return (
    <Link to={to} style={{ textDecoration: "none" }}>
      <Button variant="contained" color="primary" {...rest}>
        {children}
      </Button>
    </Link>
  );
};

export default StyledLink;
