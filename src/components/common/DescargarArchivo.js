import React from "react";
import { Button } from "@material-ui/core";

const DescargarArchivo = ({ text, SK, fileName, ...extra }) => {
  const fileId = SK.slice(3);
  const baseUrl = "https://gon-notarial.s3.amazonaws.com/public";

  return (
    <Button
      color="primary"
      href={`${baseUrl}/${fileId}/${fileName}`}
      target="_blank"
      {...extra}
    >
      {text}
    </Button>
  );
};

export default DescargarArchivo;
