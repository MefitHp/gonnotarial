import React, { useState } from "react";
import { Flex, Text, Box } from "rebass";
import { TextField, Button } from "@material-ui/core";
import { useMutation } from "react-apollo-hooks";
import { UPDATE_CASE_FILE } from "../Expedientes/Graphql/Mutations";
import { useSnackbar } from "notistack";
import { GET_CASE_FILE } from "../Expedientes/Graphql/Query";

const NarrarExpediente = ({ SK, narration = "" }) => {
  const [loading, setIsLoading] = useState(false);
  const [narrate, setNarrate] = useState(narration);
  const { enqueueSnackbar } = useSnackbar();
  const updateCF = useMutation(UPDATE_CASE_FILE);
  console.log(narration);
  const updateNarration = () => {
    setIsLoading(true);
    const args = JSON.stringify({ SK, narration: narrate });
    updateCF({
      variables: { args },
      refetchQueries: [{ query: GET_CASE_FILE, variables: { SK } }]
    })
      .then(data => {
        enqueueSnackbar("Narración actualizada", {
          variant: "success"
        });
        return setTimeout(() => {
          setIsLoading(false);
        }, 2200);
      })
      .catch(err => {
        console.error(err);
        enqueueSnackbar("Ocurrió un error :(", {
          variant: "success"
        });
        return setIsLoading(false);
      });
  };
  return (
    <Flex width="100%" flexDirection="column">
      <Text as="p">
        Este es el contenido actual de la narración, edita su contenido y da
        click en el botón guardar para actualizar el documento actual.
      </Text>
      <Flex width="100%" flexWrap="wrap">
        <Box width={[1, 3 / 4]}>
          <TextField
            style={{ whiteSpace: "pre-line" }}
            id="narrar-expediente"
            label="Narración del expediente"
            value={narrate}
            onChange={e => setNarrate(e.target.value)}
            multiline
            fullWidth
            margin="normal"
            variant="outlined"
            rows="4"
          />
        </Box>
        <Flex width={[1, 1 / 4]} justifyContent="center" alignItems="center">
          <Button
            color="primary"
            variant="outlined"
            disabled={narration === narrate}
            onClick={() => updateNarration()}
          >
            Guardar cambios
          </Button>
        </Flex>
        <Button disabled={loading} variant="contained" color="primary">
          Descargar documento
        </Button>
      </Flex>
    </Flex>
  );
};

export default NarrarExpediente;
