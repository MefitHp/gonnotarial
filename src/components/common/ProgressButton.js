import React, { useState } from "react";
import { Button } from "@material-ui/core";
import styled from "@emotion/styled";
import { useMutation } from "react-apollo-hooks";
import { useSnackbar } from "notistack";
import { CASE_FILE_PROGRESS } from "../Expedientes/Graphql/Mutations";
import { GET_CASE_FILE } from "../Expedientes/Graphql/Query";

const ProgressButton = ({ icon, children, SK, message = null, ...rest }) => {
  const [updating, setUpdating] = useState(false);
  const caseFileProgress = useMutation(CASE_FILE_PROGRESS);
  const { enqueueSnackbar } = useSnackbar();
  return (
    <ProgressBtn
      variant="contained"
      disabled={updating}
      onClick={() => {
        setUpdating(true);
        caseFileProgress({
          variables: { SK },
          refetchQueries: [{ query: GET_CASE_FILE, variables: { SK } }]
        })
          .then(done => {
            if (message) {
              enqueueSnackbar(message, {
                variant: "info"
              });
            }
            setUpdating(false);
            console.log("ACTUALIZADO");
          })

          .catch(err => {
            setUpdating(false);
            return console.log(err);
          });
      }}
      {...rest}
    >
      {updating ? (
        <>
          <div className="fa-1x">
            <i className="fad fa-circle-notch fa-spin" />
          </div>
        </>
      ) : (
        <>
          {children}
          <Icon className={icon} />
        </>
      )}
    </ProgressBtn>
  );
};

const ProgressBtn = styled(Button)`
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : "#3CD4A0"};
  color: #fff;
  min-width: 140px;
  min-height: 40px;
  margin: auto;
`;

const Icon = styled.i`
  padding-left: 8px;
`;

export default ProgressButton;
