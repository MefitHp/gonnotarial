import React from "react";
import { useMutation } from "react-apollo-hooks";
import { UPDATE_CASE_FILE } from "../Expedientes/Graphql/Mutations";
import { GET_CASE_FILES } from "../Expedientes/Graphql/Query";
import { Tooltip, Fab } from "@material-ui/core";

const BorrarExpediente = ({ args }) => {
  const updateCF = useMutation(UPDATE_CASE_FILE);
  return (
    <Tooltip title="Eliminar expediente" aria-label="Eliminar expediente">
      <Fab
        color="secondary"
        style={{
          margin: 4,
          width: 30,
          height: 30,
          minHeight: 0
        }}
        onClick={() => {
          let response = window.confirm("¿Estás seguro?");
          if (response)
            updateCF({
              variables: { args },
              refetchQueries: [{ query: GET_CASE_FILES }]
            });
          else return;
        }}
      >
        <i className="fas fa-trash" />
      </Fab>
    </Tooltip>
  );
};

export default BorrarExpediente;
