import React from "react";
import { useQuery } from "react-apollo-hooks";
import { GET_LOGS } from "./GraphQL/Query";
import DashboardLayout from "../Dashboard/DashboardLayout";
import LogCard from "./LogCard";
import { Wrapper } from "../common/common.styled";
const Bitacora = ({ match }) => {
  const { SK } = match.params;
  const { loading, error, data } = useQuery(GET_LOGS, {
    variables: { SK: `CF-${SK}` },
    fetchPolicy: "cache-and-network"
  });
  console.log(data);
  const logs = (data && data.getLogs) || [];

  return (
    <DashboardLayout>
      <Wrapper>
        {logs.map((log, index) => (
          <LogCard key={index} {...log} />
        ))}
      </Wrapper>
    </DashboardLayout>
  );
};

export default Bitacora;
