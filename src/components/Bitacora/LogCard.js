import React from "react";
import styled from "@emotion/styled";
import { Box, Text } from "rebass";
import { Paper } from "@material-ui/core";
import moment from "moment";

const LogCard = ({
  title,
  log,
  message,
  updatedAt,
  isPublic,
  role = "admin",
  ...rest
}) => {
  const date = moment.unix(updatedAt).format("DD MMMM");
  const time = moment.unix(updatedAt).format("hh:mm A");

  return (
    <Container px={4} py={2} width={[1]} {...rest}>
      <Date>
        <div>
          <i className="fad fa-calendar" />
          <b>{date}</b> <span>{time}</span>
        </div>
      </Date>
      <Card>
        <CardTitle fontSize={[1, 2, 3]} fontWeight="bold">
          {title}
        </CardTitle>
        <p>{message}</p>
        {role === "admin" && <b style={{ fontSize: 12 }}>{log}</b>}
      </Card>
      <Dot className={isPublic ? "far fa-dot-circle" : "fad fa-lock"} />
    </Container>
  );
};

const Container = styled(Box)`
  position: relative;
  display: flex;
  flex-direction: column;
  border-left: 1px solid hsl(0, 0%, 0%, 0.3);
  padding-bottom: 2.5rem;
`;
const Dot = styled.i`
  position: absolute;
  left: -8px;
  color: #20639b;
  top: calc(50% - 10px);
  background: #f6f7ff;
  font-size: 16px;
`;

const Date = styled.div`
  margin-bottom: 4px;
  b {
    font-weight: 250px;
    margin: 0 10px 0 5px;
    text-transform: uppercase;
  }
  i {
    font-size: 12px;
  }
  div {
    padding-left: 10px;
    display: flex;
    align-items: center;
    font-weight: 250px;
    font-size: 16px;
    color: rgba(0, 0, 0, 0.5);
  }
  span {
    margin: auto 0;
    font-size: 12px;
  }
`;

const Card = styled(Paper)`
  border-radius: 8px;
  padding: 16px 24px;
  display: flex;
  flex-direction: column;
`;

const CardTitle = styled(Text)`
  color: rgba(0, 0, 0, 0.75);
  padding-bottom: 4px;
`;

export default LogCard;
