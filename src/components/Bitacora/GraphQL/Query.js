import gql from "graphql-tag";

export const GET_LOGS = gql`
  query getLogs($SK: ID!) {
    getLogs(SK: $SK) {
      PK
      SK
      title
      message
      log
      isPublic
      updatedAt
    }
  }
`;
