import React from "react";
import { Link } from "react-router-dom";
import { Chip, Button, Tooltip, Fab } from "@material-ui/core";
import { Box } from "rebass";
import BorrarExpediente from "../components/common/BorrarExpediente";

export function handleFileStatus(phase) {
  switch (phase) {
    case "CREATED":
      return (
        <CustomChip
          phase="CREADO"
          // icon="Manager"
        />
      );
    // case "NOTARY_PRESENTED":
    //   return <CustomChip phase="Presentado con notario" icon="Notaria" />;
    // case "COURT_PRESENTED":
    //   return <CustomChip phase="Presentado en Juzgado" icon="Juzgado" />;

    default:
      return <CustomChip phase={phase} icon="Notaria" />;
  }
}

export function handleTableColumns(rol) {
  const common = [
    {
      title: "Fase",
      field: "phase",
      render: tableData => {
        let { phase } = tableData;
        return handleFileStatus(phase);
      }
    },
    { title: "Expediente", field: "fileNumber" },
    {
      title: "Cliente",
      field: "actorId",
      render: tableData => {
        let actorData = JSON.parse(tableData.actorData);
        return <p>{actorData.fullName}</p>;
      }
    }
  ];

  switch (rol) {
    case "LAW_FIRM":
      return [
        ...common,
        { title: "Demandado", field: "defendant" },
        // {
        //   render: tableData => {
        //     let { data } = tableData;
        //     let date = moment
        //       .unix(data.split("#")[1])
        //       .format("DD/MMMM/YY LT a");
        //     return date;
        //   }
        // },
        {
          title: "Acciones",
          field: "SK",
          render: tableData => {
            let { SK } = tableData;
            let args = JSON.stringify({ SK, isDeleted: true });

            return (
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <Link
                  to={`/dashboard/expediente/${SK}`}
                  style={{ textDecoration: "none" }}
                >
                  <Button color="primary">
                    <i className="fas fa-eye" /> <Box px={1} />
                    Ver
                  </Button>
                </Link>
                <BorrarExpediente args={args} />
              </div>
            );
          }
        }
      ];
    case "CONTROL_TABLE":
      return [
        ...common,
        {
          title: "Acciones",
          field: "SK",
          render: tableData => {
            let { SK } = tableData;
            return (
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <Link
                  to={`/dashboard/expediente/${SK}`}
                  style={{ textDecoration: "none" }}
                >
                  <Button color="primary">Ir al expediente</Button>
                </Link>
                <Tooltip
                  title="Eliminar expediente"
                  aria-label="Eliminar expediente"
                >
                  <Fab
                    color="secondary"
                    style={{
                      margin: 4,
                      width: 30,
                      height: 30,
                      minHeight: 0
                    }}
                  >
                    <i className="fas fa-trash" />
                  </Fab>
                </Tooltip>
              </div>
            );
          }
        }
      ];

    default:
      return;
  }
}

let CustomChip = ({ phase, icon = "", size = "small" }) => {
  let displayIcon = "";
  if (icon.toLowerCase() === "notaria") displayIcon = "fas fa-building";
  else if (icon.toLowerCase() === "manager") displayIcon = "fas fa-briefcase";
  else if (icon.toLocaleLowerCase() === "") displayIcon = "";
  else displayIcon = "fas fa-gavel";
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Chip
        style={{ margin: 5, backgroundColor: "rgba(32,99,155, 0.2)" }}
        label={
          <span style={{ fontWeight: 500 }}>
            {phase} <i style={{ marginLeft: 5 }} className={displayIcon} />
          </span>
        }
        size={size}
      />
    </div>
  );
};
