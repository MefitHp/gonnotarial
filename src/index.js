import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { ThemeProvider } from "@material-ui/styles";
import Routes from "./Routes";
import * as serviceWorker from "./serviceWorker";
import theme from "./utils/theme";
import Amplify, { Auth } from "aws-amplify";
import awsConfig from "./aws-exports";
import AWSAppSyncClient, { AUTH_TYPE } from "aws-appsync";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import { SnackbarProvider } from "notistack";
import "moment/locale/es";

Amplify.configure(awsConfig);

const client = new AWSAppSyncClient({
  url: awsConfig.graphqlEndpoint,
  region: awsConfig.region,
  auth: {
    type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
    jwtToken: async () =>
      (await Auth.currentSession()).getIdToken().getJwtToken()
  }
});

let App = () => (
  <>
    <SnackbarProvider
      maxSnack={5}
      anchorOrigin={{ vertical: "top", horizontal: "center" }}
      autoHideDuration={3000}
    >
      <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <ThemeProvider theme={theme}>
            <Routes />
          </ThemeProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    </SnackbarProvider>
  </>
);

ReactDOM.render(<App />, document.getElementById("root"));
serviceWorker.unregister();
